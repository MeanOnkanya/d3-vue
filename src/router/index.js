import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Chart from '@/components/Chart'
import Pie from '@/components/pie-chart'
import Sunburst from '@/components/sunburst-chart'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/HelloWorld',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/',
      name: 'Chart',
      component: Chart
    },
    {
      path: '/Pie',
      name: 'Pie',
      component: Pie
    },
    {
      path: '/Sunburst',
      name: 'Sunburst',
      component: Sunburst
    }

  ]
})
